var services = angular.module('services', []);
var stuffApp = angular.module('stuffApp', ['services']);

stuffApp.controller('BaseController', function ($scope, cartService, stockService) {
	/*
	* set product list and user products accessible on scope from respective services
	* productList and userProducts are simple frequency hashes mapping
	* product as the keys to price/stock or quantity added respectively
	*/
  	$scope.productList = stockService.productList;
  	$scope.userProducts = cartService.cart
});
