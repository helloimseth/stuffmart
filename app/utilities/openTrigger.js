stuffApp.directive('openTrigger', function () {
  return {
    link: function (scope, elem, attrs) {
      var openClass = attrs.openClass,
          $trigger = angular.element(elem[0].querySelector('#trigger')),
          $close = angular.element(elem[0].querySelector('#close'));

      $trigger.on('click', function (e) {
        e.preventDefault();

        elem.toggleClass(openClass);
      });

      $close.on('click', function (e) {
        e.preventDefault();

        elem.removeClass(openClass);
      });
    }
  };
});
  

