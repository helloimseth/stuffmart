stuffApp.directive('productList', [function ($scope, cartService, stockService) {
  return {
    restrict: 'A',
    replace: true,
    controller: ListController,
    templateUrl: 'build/templates/productList.html'
  };
}]);


function ListController ($scope, cartService, stockService) {
  
  $scope.addToCart = function (productName, qty) {
    // ensure qty is valid
    if (!qty) {
      return;
    }

    var product = $scope.productList[productName],
        productInStock = !!product && product.stock >= qty;

    // if product exists, add to users productList and update stock count on productList
    if (qty > 0 && productInStock) {
      cartService.addToCart(productName, qty);
      stockService.updateStockCount(productName, qty)
    }
  }
}