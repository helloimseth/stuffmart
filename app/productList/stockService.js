services.service('stockService', function() {
  this.productList = { };

  var that = this;

  this.addToStock = function (product) {
  	if (_isInStock(product.name)) {
  		that.updateStockCount(product.name, product.stock * -1);
  	} else {
  		that.productList[product.name] = {
	      price: product.price,
	      stock: product.stock
	    };	
  	}
  }

  /*
    parameters: name of product, amount to add to cart
    
    updates stock count in productList
  */
  this.updateStockCount = function(productName, amount) {
    that.productList[productName].stock -= amount;
  }

  function _isInStock (productName) {
  	return that.productList.hasOwnProperty(productName);
  }
});
