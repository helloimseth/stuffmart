stuffApp.directive('productForm', function (stockService) {
  return {
    restrict: 'A',
    scope: false,
    controller: FormController,
    templateUrl: 'build/templates/productForm.html'
  };
});
  
function FormController ($scope, stockService) {
  $scope.product = {}

  $scope.addProduct = function (product) {
    $scope.productForm.$setSubmitted();

    if ($scope.productForm.$valid) {
      stockService.addToStock(product);
    
      _resetForm();  
    }
  }

  function _resetForm () {
    $scope.productForm.$setUntouched();
    $scope.productForm.$setPristine();

    $scope.product = {};
  }
}

