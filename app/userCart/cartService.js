services.service('cartService', function() {
  this.cart = {};

  var that = this;

  /*
  checks if cart is empty
  */
  this.isEmpty = function () {
    var empty = true;

    for (var product in that.cart) {
      if (that.cart.hasOwnProperty(product)) {
        empty = false;
      }
    }

    return empty;
  }

  /*
  parameters: name of product, amount to add to cart

  private function to check if product is already in cart, 
  updates cart accordingly
  */
  this.addToCart = function(productName, qty) {
    if (that.productInCart(productName)) {
      that.cart[productName] += qty;
    } else {
      that.cart[productName] = qty;
    }
  }

  /*
  parameters: name of product

  removes all count of product from cart
  */
  this.removeFromCart = function (productName) {
    delete that.cart[productName];
  }

  /*
  parameters: name of product

  private function to test if product is in cart
  */
  this.productInCart = function(productName) {
    var count = that.cart[productName];

    return typeof count === "number" && count > 0;
  }
});