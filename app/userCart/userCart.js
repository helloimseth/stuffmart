stuffApp.directive('userCart', [function ($scope, cartService, stockService) {
  return {
    restrict: 'A',
    replace: true,
    scope: false,
    controller: CartController,
    templateUrl: 'build/templates/userCart.html'
  };
}]);

function CartController ($scope, cartService, stockService) {

  $scope.removeFromCart = function (productName) {
    if (cartService.productInCart(productName)) {
      stockService.updateStockCount(productName, $scope.userProducts[productName] * -1);
      cartService.removeFromCart(productName);
    }
  }

  $scope.cartIsEmpty = cartService.isEmpty;
}