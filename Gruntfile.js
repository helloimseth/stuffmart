/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    sass: {
      dev: {
        options: {
          sourcemap: 'none',
          style: 'compressed',
        },
        files: {
         'build/application.css': 'assets/scss/application.scss'
        }
      }
    },
    uglify: {
      dev: {
       options: {
          mangle: false,
          beautify: true,
          compress: false,
          preserveComments: true
        },
        files: {
            'build/app.js' : [
              'app/**/*.js',
              'app/*.js'
        ]}
      }
    },
    copy: {
      dev: {
        files: [{
          expand: true, 
          flatten: true,
          cwd: 'app', 
          src: ['**/*.html'], 
          dest: 'build/templates/'
        }],
      }
    },
    watch: {
      html: {
        files: ['app/**/*.html'],
        tasks: ['copy:dev']
      },
      js: {
        files: ['app/**/*.js', 'app/*.js'],
        tasks: ['uglify:dev']
      },
      scss: {
        files: ['assets/scss/*.scss', 'assets/scss/**/*.scss'],
        tasks: ['sass:dev']
      }
    },
    browserSync: {
      dev: {
          bsFiles: {
              src : [
                  '*.html',
                  'build/application.css',
                  'build/app.js',
                  'templates/*.html'
              ]
          },
          options: {
              watchTask: true,
              server: './'
          }
      },
      dist: {
        options: {
            server: './'
        }
      }
    },
  });

  var gruntTasks = ['grunt-contrib-watch', 'grunt-browser-sync', 'grunt-contrib-sass', 'grunt-contrib-uglify', 'grunt-contrib-copy'];

  // These plugins provide necessary tasks.
  gruntTasks.forEach(function (task) {
    grunt.loadNpmTasks(task);
  });

  // Default task.
  grunt.registerTask('default', ['browserSync', 'watch']);
  grunt.registerTask('dev', [ 'browserSync:dev', 'sass:dev', 'uglify:dev', 'copy:dev', 'watch']);
  grunt.registerTask('build', [ 'sass:dev', 'uglify:dev', 'copy:dev', 'browserSync:dist']);
};
