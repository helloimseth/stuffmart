# Stuffmart #

Small web app for:

* Displaying List of products
* Adding products to product list
* Adding products to user cart
* Remove products from user cart

### To run ###
The app is compiled by Grunt tasks and served through BrowserSync. After cloning the repo and navigating to the root folder in terminal, run:

```
#!bash

npm install
grunt build
```

This will open the app in the default browser on the localhost.

### Implementation notes ###
The cart and product stock are held in different angular services. Using the singleton status of services, this allows each to hold the data cleanly and have an globally accessible API without clogging the $scope. On a project with a backend, it would make sense to have these services perform the API calls to the server, probably using a separate service owning these AJAX functions. 

I attached the data objects from the services directly on the $scope to be accessible in the DOM, however, also if this project included a server-side API, I would more likely keep separate objects on the $scope that are populated using the calls from the respective services.

The services are attached to a services module injected into the main stuffApp module, keeping the architecture more modular. The directives for the various components of the project are attached to the primary stuffApp module, however on a larger App, I think it would make sense to separate these into component-based modules for cleaner file structure and modularity.

### SCSS Notes ###
The directory structure is a simplified version of SMACSS and the class-naming follows the basic ideas of BEM, adding prefixes to denote the hierarchy (please see below). 

* assets
  * scss
    * global - global objects and styles, such as resets, or the header (prefixed with g-)
    * modules - base modules (prefixed with m-)
    * state - states of modules, similar to modules but contain + overwrite child modules (prefixed with s-)
  * scss files (variables, mixins, placeholders)
  * manifest scss file

### To do ###
Validations! Will add this weekend.